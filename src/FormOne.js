import React, { useState } from 'react';

import styles from './App.css';

function FormOne() {
	const [ state, setState ] = useState({
		name: '',
		email: '',
		meal: '',
		isGoing: ''
	});

	function handleSubmit(e) {
		e.preventDefault();
		alert(state.name + ' | ' + state.email + ' | ' + state.meal + ' | ' + state.isGoing);
	}

	function handleChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		setState({
			...state,
			[name]: value
		});
	}

	return (
		<div className={styles.App}>
			<form className={styles.form} onSubmit={handleSubmit}>
				<h4 className={styles.formTitle}>
					Add Guest
					<hr />
				</h4>
				<div className={styles.formGroup}>
					<label htmlFor="name">Full Name</label>
					<input type="text" name="name" value={state.name} onChange={handleChange} />
				</div>

				<div className={styles.formGroup}>
					<label htmlFor="email">Email</label>
					<input type="email" name="email" value={state.email} onChange={handleChange} />
				</div>

				<div className={styles.inlineGroup}>
					<div className={styles.formGroup}>
						<label htmlFor="meal">Meal Preference</label>
						<select name="meal" value={state.meal} onChange={handleChange}>
							<option value="1">Jollof Rice</option>
							<option value="2">Fried Rice</option>
							<option value="3">Pork</option>
							<option value="4">Chicken</option>
							<option value="5">Beef</option>
						</select>
					</div>

					<div className={styles.formGroup}>
						<label htmlFor="meal">Is Going?</label>
						<input name="isGoing" type="checkbox" value={state.isGoing} onChange={handleChange} />
					</div>
				</div>
				<div className={styles.formGroup}>
					<button type="submit">Submit</button>
				</div>
			</form>
		</div>
	);
}

export default FormOne;
