import React from 'react';

const Field = ({ onChange, onBlur, name, label, type, error }) => (
	<div>
		<label>{label}</label>
		<div>
			<input
				style={error ? { backgroundColor: '#ffe6e6' } : {}}
				name={name}
				onChange={onChange}
				placeholder={label}
				type={type}
				onBlur={onBlur}
			/>
			{error && <span style={{ color: 'red' }}>{error}</span>}
		</div>
	</div>
);

export default Field;
