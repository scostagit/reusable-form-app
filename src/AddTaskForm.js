import React from 'react';
import useForm from './components/form/useForm';
import Field from './components/form/Field';

function AddTaskForm() {
	const taskState = {
		name: '',
		description: ''
	};

	function onSubmit(values, error) {
		if (error === null) {
			alert(JSON.stringify({ values }, null, 2));
		}
	}

	function validate(values) {
		const errors = {};
		if (values.name === '') {
			errors.name = 'Required';
		} else if (values.name.length > 11) {
			errors.name = 'max length 11';
		} else {
			errors.name = '';
		}

		if (values.description === '') {
			errors.description = 'Required';
		} else if (values.description.length > 100) {
			errors.description = 'max length 100';
		} else {
			errors.description = '';
		}

		return errors;
	}

	const { values, handleBlur, handleChange, handleSubmit, errors } = useForm({
		initialValues: taskState,
		onSubmit,
		validate
	});

	return (
		<div>
			<form onSubmit={handleSubmit}>
				<h4>
					Add Task
					<hr />
				</h4>
				<div>
					<Field
						type="text"
						name="name"
						label="Task Name"
						value={values.name}
						onChange={handleChange}
						onBlur={handleBlur}
						error={errors.name}
					/>
				</div>

				<div>
					<Field
						type="text"
						name="description"
						label="Description"
						value={values.description}
						onChange={handleChange}
						onBlur={handleBlur}
						error={errors.description}
					/>
				</div>

				<div>
					<button type="submit">Save</button>
				</div>
			</form>
		</div>
	);
}

export default AddTaskForm;
