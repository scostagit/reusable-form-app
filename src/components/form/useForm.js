import React from 'react';
const useForm = ({ initialValues, onSubmit, validate }) => {
	const [ values, setValues ] = React.useState(initialValues || {});
	const [ touchedValues, setTouchedValues ] = React.useState({});
	const [ errors, setErrors ] = React.useState({});

	const handleChange = (event) => {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		setValues({
			...values,
			[name]: value
		});
	};

	const handleBlur = (event) => {
		const target = event.target;
		const name = target.name;
		setTouchedValues({
			...touchedValues,
			[name]: true
		});
		const currentErrors = validate(values);
		setErrors({
			...errors,
			...currentErrors
		});
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		const currentErrors = validate(values);
		setErrors({
			...errors,
			...currentErrors
		});

		let hasError = false;
		for (var property in currentErrors) {
			if (currentErrors[property] !== '') {
				hasError = true;
			}
		}
		onSubmit(values, hasError ? currentErrors : null);
	};

	return {
		values,
		touchedValues,
		errors,
		handleChange,
		handleSubmit,
		handleBlur
	};
};

export default useForm;
