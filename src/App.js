import React from 'react';
import useForm from './components/form/useForm';
import Field from './components/form/Field';
import styles from './App.css';

function App() {
	const useState = {
		name: '',
		email: '',
		meal: '',
		isGoing: false
	};

	function onSubmit(values, error) {
		if (error === null) {
			alert(JSON.stringify({ values }, null, 2));
		}
	}

	function validate(values) {
		const errors = {};
		if (values.name === '') {
			errors.name = 'Required';
		} else if (values.name.length > 11) {
			errors.name = 'max length 11';
		} else {
			errors.name = '';
		}

		if (!values.email) {
			errors.email = 'Required';
		} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
			errors.email = 'Invalid email address';
		} else {
			errors.email = '';
		}

		if (errors.meal === false) {
			errors.meal = 'Required';
		} else {
			errors.meal = '';
		}

		return errors;
	}

	const { values, handleBlur, handleChange, handleSubmit, errors } = useForm({
		initialValues: useState,
		onSubmit,
		validate
	});

	return (
		<div className={styles.App}>
			<form className={styles.form} onSubmit={handleSubmit}>
				<h4 className={styles.formTitle}>
					Add Guest
					<hr />
				</h4>
				<div className={styles.formGroup}>
					<Field
						type="text"
						name="name"
						label="Full Name"
						value={values.name}
						onChange={handleChange}
						onBlur={handleBlur}
						error={errors.name}
					/>
				</div>

				<div className={styles.formGroup}>
					<Field
						type="text"
						name="email"
						label="Email"
						value={values.email}
						onChange={handleChange}
						onBlur={handleBlur}
						error={errors.email}
					/>
				</div>

				<div className={styles.inlineGroup}>
					<div className={styles.formGroup}>
						<label htmlFor="meal">Meal Preference</label>
						<select name="meal" value={values.meal} onChange={handleChange}>
							<option value="1">Jollof Rice</option>
							<option value="2">Fried Rice</option>
						</select>
					</div>

					<div className={styles.formGroup}>
						<Field
							type="checkbox"
							name="isGoing"
							label="Is Going?"
							value={values.isGoing}
							onChange={handleChange}
							onBlur={handleBlur}
							error={errors.meal}
						/>
					</div>
				</div>

				<div className={styles.formGroup}>
					<button type="submit">Submit</button>
				</div>
			</form>
		</div>
	);
}

export default App;
